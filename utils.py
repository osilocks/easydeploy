import socket, os, sys
from pbs import which, Command


ERROR_FILE = "easyweb_error.txt"
#Fix to accomodate apple package installer
if "darwin" in os.sys.platform:
	# apt_get = Command(which("brew"))
	apt_get = lambda action, package: Command(which(package))
else:
	from pbs import apt_get

def gethostname():
	fullname = socket.gethostname()
	if '.' not in fullname:
		fullname = resolve(fullname)
	return fullname

def fsist(folder):
	return os.path.exists(folder) 

def get_command(command, package):
	# if not which(command): 
	# 	apt_get("install", package) 
	return Command(which(command))

def get_command_or_pip(package, command):
	if not which(command):
		Command("pip")("install", package)
	return Command(which(command))

def path_exists(path):
	def dec(target):
		def wrapper(*args, **kwargs):
			if os.path.exists(path):
				return target(*args, **kwargs)
			else:
				raise(Exception("File path <%s> required" % path))
		return wrapper
	return dec

def require_key(key):
	def dec(target):
		def wrapper(self, *args, **kwargs):
			if key in self.user.keys():
				return target(self, *args, **kwargs)
			else:
				raise Exception("Missing required key: %s" % key)
		return wrapper
	return dec

def require_keyas(key, value):
	def dec(target):
		def wrapper(self, *args, **kwargs):
			if key in self.user.keys():
				if self.user[key] == value:
					return target(self, *args, **kwargs)
			else:
				raise Exception("Missing required key %s, key should be %s" % (key, value))
		return wrapper
	return dec

def require_keys(keys):
	def dec(target):
		def wrapper(self, *args, **kwargs):
			req_keys = []
			for key in keys:
				if key in self.user.keys():
					continue
				else:
					req_keys.append(key)
			if len(req_keys):
				raise Exception("Missing required key(s: %s" % ", ".join(req_keys))
			return target(self, *args, **kwargs)
		return wrapper
	return dec

def _sysargs(argv=None):

	"""Command-line -> method call arg processing.

	

	- positional args:

			a b -> method('a', 'b')

	- intifying args:

			a 123 -> method('a', 123)

	- json loading args:

			a '["pi", 3.14, null]' -> method('a', ['pi', 3.14, None])

	- keyword args:

			a foo=bar -> method('a', foo='bar')

	- using more of the above

			1234 'extras=["r2"]'  -> method(1234, extras=["r2"])

	

	@param argv {list} Command line arg list. Defaults to `sys.argv`.

	@returns (<method-name>, <args>, <kwargs>)

	"""

	import json

	import sys

	if argv is None:

		argv = sys.argv

	arg_strs = argv[1:]

	args = []

	kwargs = {}

	for s in arg_strs:

		if s.count('=') == 1:

			key, value = s.split('=', 1)

		else:

			key, value = None, s

		try:

			value = json.loads(value) 

		except ValueError:

			pass

		if key:

			kwargs[key] = value

		else:

			args.append(value)

	return args, kwargs
