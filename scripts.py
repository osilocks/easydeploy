import os, sys
from pbs import ls, cd, mkdir, git, cat, which, rm, mv, echo, sudo
import pbs
from utils import *

class BaseScript(object):
	def __init__(self, user, *args, **kwargs):
		self.user = user
		if user.has_key("venv"):
			python = os.path.join(self.user["home"], "work", "bin", "python")
		else:
			python = pbs.Command(which("python"))
		self.python =  pbs.Command(python)

	def init_database(self):
		cd(self.project_dir)
		self.python("manage.py", "syncdb", "--noinput", _err=ERROR_FILE)
		self.python("manage.py", "migrate", "--noinput", _err=ERROR_FILE)
		self.python("manage.py", "collectstatic", "--noinput", _err=ERROR_FILE) 

	def local_settings(self):
		return ("generic/local_settings.py", {})
			
	def getDir(self): 
		self.block = os.path.join(self.user["home"], self.user.get("block")) 
		self.project_dir = os.path.join(self.block, 'project')
		
		if not os.path.exists("%s/manage.py" % self.project_dir):
			self.createDir()
		print "Block Created <%s>" % self.project_dir

	def createDir(self):
		raise NotImplementedError("Directory creation must be implemented in derived classes")


class EasywebScript(BaseScript):
	deploy = pbs.Command(which("easyweb_deploy.py"))

	def createDir(self):
		with cd(self.user["home"]):
			deploy("block")


class MezzanineScript(BaseScript):
	deploy = get_command_or_pip("mezzanine", "mezzanine-project")	

	def createDir(self): 
		project_dir = self.project_dir
		rm("-rf", project_dir)
		print "Creating Mezzanine block <%s>" % project_dir
		self.deploy("block")

	def init_database(self):
		cd(self.project_dir)
		self.python("manage.py", "createdb", "--noinput", _err=ERROR_FILE)
		self.python("manage.py", "collectstatic", "--noinput", _err=ERROR_FILE)

	def local_settings(self):
		return ("mezzanine/local_settings.py", {})

	def settings_path(self):
		return "settings.py"
	
	def serve_folders(self):
		return (
			("static", "%s/static" % self.user["block"]),
		)


class DjangoScript(BaseScript):
	deploy = pbs.Command(which("django-admin.py"))

	def createDir(self): 
		project_dir = self.project_dir  
		rm("-rf", project_dir)
		rm("-rf", self.block+'/manage.py')

		print "Creating django block <%s>" % project_dir
		self.deploy("startproject", "project", self.block) 

	def init_database(self):
		cd(self.block)
		self.python("manage.py", "syncdb", "--noinput", _err=ERROR_FILE)
		self.python("manage.py", "migrate", "--noinput", _err=ERROR_FILE)
		self.python("manage.py", "collectstatic", "--noinput", _err=ERROR_FILE) 

	def settings_path(self):
		return "settings.py"

	def serve_folders(self):
		return (
			("static/media", "%s/static/media" % self.user["block"]),
		)


class GitScript(BaseScript):
	deploy = git

	def createDir(self): 
		project_dir = self.project_dir
		rm("-rf", project_dir) 
		current_user = pbs.Command('whoami')() 
		print "Creating Git block <%s> using user<%s>" % (project_dir, current_user)

		repo = self.user["repo"] 
		print "Cloning Repo <%s>" % repo
		self.deploy("clone", repo, self.project_dir)  
		
	def serve_folders(self):
		return (
			("static/media", "%s/static/media" % self.user["block"]),
		)

	def settings_path(self):
		return "settings.py"

class MercurialScript(BaseScript):
	deploy = pbs.Command(which("hg")) 
			
	def createDir(self):
		repo = self.user["repo"]
		print "Cloning Repo <%s>" % repo
		deploy("clone", repo, self.project_dir) 
		return self.project_dir
