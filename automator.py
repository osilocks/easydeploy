from pbs import ls, cd, mkdir, pwd, \
	git, rm, mv, cat, echo, chown, chgrp, chown, sudo, touch
from mako.lookup import TemplateLookup
import pbs, platform

from author import UserDetails
from scripts import GitScript, DjangoScript, \
		MercurialScript, EasywebScript, MezzanineScript

from utils import *
import socket
import fnmatch
from os.path import *

virtualenv = get_command("virtualenv", "python-virtualenv")




class Automator(object):

	mylookup = TemplateLookup(directories=[os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")])
	deployscripts = {
		"easyweb": EasywebScript,
		"local" : "",
		"django" : DjangoScript,
		"git": GitScript,
		"mercurial": MercurialScript,
		"mezzanine": MezzanineScript,
	}
	envname = "work"
	nginx_sites_folder = "/etc/nginx/sites-available"
	gunicorn_init_folder = "/etc/init"
	user=None

	def __init__(self, *args, **kwargs):
		touch(ERROR_FILE)
		if kwargs:
			for key, value in kwargs.iteritems(): 
				try:
					setattr(self, key, value)
				except Exception:
					pass
		if args:
			# self.user = UserDetails(**user)
			if "deploy" in args[0]:
				if not self.user.has_key("port"):
					self.assignPort()

				print "Deploying user with values %s" % self.user
				self.user["venv"] = True
				self.deploy()
			elif "upgrade" in args[0]:
				self.upgrade()
			self.project_dir = None
		else:
			return

	@require_keys(("home", "deploy_type"))
	def deploy(self):
		self.user["home"] = abspath(self.user["home"])
		if os.path.exists(self.user["home"]):
			deploy_type = self.user['deploy_type'].lower()
			print "Deploying Django App using %sScript for User <%s>" % (deploy_type, self.user["username"])
			self.script = self.deployscripts.get(deploy_type.lower(), None)
			if self.script is None:
				raise Exception("Unknown Deploy Type")
						
			self.script = self.script(self.user)
			self.createBlock()
			if os.path.exists(self.script.project_dir): 
				self.user["project"] = self.script.project_dir

			if self.user["venv"]:
				self.user["venv_path"] = self.virtualenv()
				self.cow(self.user["venv_path"])
			#install requirements into virtual env
			self.requirements()
			#Make gunicorn config if needed
			self.gunicorn()
			#update local settings if needed
			self.updateLocalSettings()
			#change ownership to user
			self.cow(self.script.project_dir)
			#initilize database
			self.database()

			#create nginx files
			self.serve()
		else:
			print "Home directory <%s> does not exist" % self.user["home"]
			return

	def cow(self, folder):
		if os.path.exists(folder):
			chown("-R", self.user["username"], folder, _err=ERROR_FILE)
			chgrp("-R", self.user["username"], folder, _err=ERROR_FILE)

	@require_key("home")
	def virtualenv(self):
		"""
		Create a venv for the user
		"""
		venv_path = os.path.join(self.user["home"], self.envname)

		if not os.path.exists(venv_path):
			print "Creating Virtual Environment <%s>" % venv_path
			virtualenv("--distribute", venv_path )

		print "Virtual Environment Created <%s>" % venv_path
		return venv_path

	@require_key("home")
	def createBlock(self):
		"""
		Creates a block directory which stores the projects code
		"""
		try:
			self.user["block"] = "%s/block" % (self.user["home"]) 
			cd(abspath(self.user["home"]))
			mkdir('-p', self.user["block"])
			self.script.getDir()
		except Exception:
			raise

	def assignPort(self):
		"Assign port from acceptable range"
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.bind(('', 0))
		port = s.getsockname()[1]
		s.close()
		self.user["port"] = port
		# self.user["fqdn"] = gethostname()

	def install_requirements_file(file): 
		print pip("install",  r, file, _err=ERROR_FILE)

	# @require_key("packages")
	def requirements(self):
		"""
		Calls pip install -r requirements for the client environment
		"""
		home = self.user["home"]
		"change to virtualenv if virtualenv exists"
		if self.user.has_key("venv_path"):
			venv_path = self.user["venv_path"]
			cd(self.user["home"]) 
			with sudo("-u", self.user["username"], _with=True, _err=ERROR_FILE):
				"assume pip is installed"
				pip = pbs.Command("work/bin/pip")
				packages = []
				if self.user.has_key("packages"):
					if len(self.user["packages"]) > 1:
						packages = self.user["packages"].split(",")
				if self.user["dbtype"] == "postgres":
					packages.append("psycopg2")
				for req in packages + ["django", "gunicorn==0.13.4"]:
					print "Installing %s requirement" % req
					print pip("install",  req, _err=ERROR_FILE)

				#install requirements that may be in the block folder 
				for req_file in os.listdir('.'):
					if fnmatch.fnmatch(req_file, '*requirements*'):
						# block_requirements = join(self.user["project"], "requirements")
						if isfile(req_file):
							try:
								print pip("install",  r, req_file, _err=ERROR_FILE)
							except Exception:
								pass
						elif isdir(req_file):
							for (path, dirs, files) in os.walk(req_file):
								for afile in files:
									print pip("install",  "-r", os.path.join(path, afile), _err=ERROR_FILE)
				self.cow(self.user["venv_path"]) 
				print "Installed requirements into Virtual Environment <%s>" % venv_path

	

	@require_keyas("override_local_settings", "Yes")
	def updateLocalSettings(self):
		"""
		Updates the blocks local settings with user info, like database
		"""
		print "Updating Local Settings"
		try:
			(template, extra_vars) = self.script.local_settings()
			local_settings = self.publish(template, extra_vars)
			file_path = os.path.join(self.script.project_dir, "local_settings.py")
			f = open(file_path, "w")
			f.write(local_settings)
			print ">>>>>>>>> Created Local Settings File <%s> at <%s> <<<<<<<<<<<<" % (f, file_path)
		except AttributeError:
			print "No local settings template specified"
		except Exception:
			print "Could not create local settings"

	def database(self):
		self.script.init_database()

	@require_keys(("home", "block", "serve_mode", "domain"))
	def serve(self):
		print "Serving Django App"
		if "nginx-gunicorn" in self.user["serve_mode"]:
			folders = self.script.serve_folders()
			extra = {
				"media_paths": folders,
				"domains": ["*.%s" % self.user["domain"], self.user["domain"]]
			}
			filepath = os.path.join(self.nginx_sites_folder, self.user["domain"])
			f = open(filepath, "w")
			f.write(self.publish("nginx.vhost.conf", extra))
			#restart nginx
			nginx = pbs.Command("/etc/init.d/nginx")
			if "running" in nginx("status"):
				nginx("-s reload")
				print "Reloaded Nginx"
			else:
				nginx("start")
				print "Started Nginx"

	def gunicorn(self):
		init_path = self.gunicorn_init_folder

		path = os.path.join(self.user["block"], "gunicorn")

		if not os.path.exists(path):
			mkdir(path)

		extra_vars = {
			"settings_path": self.script.settings_path(),
		}

		gun = self.publish("user.gunicorn.conf", extra_vars)
		gun_name = self.user["domain"].replace(".", "-")

		f = open(os.path.join(init_path, "%s.conf" % gun_name), "w")
		f.write(gun)

		path_to_gunpy = os.path.join(self.user["block"], "gunicorn", "gunicorn.py")
		gunpy = self.publish("gunicorn.py", extra_vars)

		f = open(path_to_gunpy, "w")
		f.write(gunpy)

		#start gunicorn service
		if "start/running" in pbs.status(gun_name):
			pbs.restart(gun_name, _err=ERROR_FILE)
		else:
			pbs.start(gun_name, _err=ERROR_FILE)


	def publish(self, template, extravars={}):
		"""
		Renders user information to template
		"""
		tmpl = self.mylookup.get_template("%s.esl" % template)
		data = self.user
		return tmpl.render(user=self.user, full=False, **extravars)


if __name__ == "__main__":
	from utils import _sysargs
	args, kwargs = _sysargs(sys.argv)
	user = {
		"username":"osiloke",
		"pass":"test",
		"dbname":"test",
		"dbpass":"test",
		"dbuser":"test",
		"home": "./osiloke",
		"deploy_type": "django",
		"serve_mode": "nginx-gunicorn",
		"dbtype": "postgres",
		"venv": True,
		"domain": "osiloke.localcom",
		# "packages": "django, mezzanine, cartridge",
		"repo": "git@bitbucket.org:osilocks/progweb_project.git",
		"override_local_settings": "Yes",

	}

	base_path = os.path.abspath( os.path.dirname(__file__) )

	class_dict = dict(
		envname = "work",
		nginx_sites_folder = os.path.join(base_path, "etc/nginx/sites-available"),
		gunicorn_init_folder = os.path.join(base_path,"etc/init")
	)
	a = Automator("deploy", user=UserDetails(**user), **class_dict)
