from pprint import pformat
from UserDict import DictMixin

class BaseModel(DictMixin, object): 
	def __init__(self, *args, **kwargs):
		self._values = {}
		if args or kwargs:
			for k, v in dict(*args, **kwargs).iteritems():
				self[k] = v

	def __getitem__(self, key):
		if key in self._values.keys():
			return self._values[key]
		return False
	
	def __add___(self, some_dict):
		values = some_dict|self._values
		return values

	def __setitem__(self, key, value):
		self._values[key] = value

	def __delitem__(self, key):
		del self._values[key]

	def __getattr__(self, name):
		if name in self._values.keys():
			raise AttributeError("Use item[%r] to get field value" % name)
		raise AttributeError(name)

	def __setattr__(self, name, value):
		if not name.startswith('_'):
			raise AttributeError("Use item[%r] = %r to set field value" % \
				(name, value))
		super(BaseModel, self).__setattr__(name, value)

	def keys(self):
		return self._values.keys()

	def has_key(self, key):
		return key in self.keys()
		
	def __repr__(self):
		return pformat(dict(self))

class UserDetails(BaseModel):
	"""
	Sets user attributes from argv
	"""

	def deploymentType(self):
		if self.has_attr("type"):
			return self["type"]
		else:
			return "easyweb"

if __name__ == "__main__":
	f = {"dbname":"db"}
	u = UserDetails(**f)
	print u.values()
	print u["ss"]