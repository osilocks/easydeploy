import os, sys

if __name__ == "__main__":
	"""
	Determine if we are in a virtualmin or cpanel environment
	"""
	from utils import _sysargs 
	args, kwargs = _sysargs(sys.argv) 

	servervars = __import__("servervars")

	if args:
		"This should be the entry type"
		keys = servervars.entry_points[args[0]] 
		user_vars = {keys[key]:value for key, value in kwargs.items() if key in keys}
		from author import UserDetails
		from automator import Automator
		a = Automator("deploy", user=UserDetails(**user_vars))