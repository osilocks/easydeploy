entry_points = {}

"""
The entry_points dict is used as a lookup for control panel variables.
e.g virtualmin variable equivalents in easydeploy.
The vars would be used to replace virtualmin keys.
"""

entry_points["virtualmin"] = {
	"VIRTUALSERVER_FIELD_DJANGO": "django",
	"VIRTUALSERVER_FIELD_SERVEMODE": "serve_mode",
	"VIRTUALSERVER_FIELD_PACKAGES": "packages",
	"VIRTUALSERVER_FIELD_REPO": "repo",
	"VIRTUALSERVER_FIELD_OVLS": "override_local_settings",
	#User data
	"VIRTUALSERVER_FIELD_FULLNAME": "fullname",
	"VIRTUALSERVER_USER": "username",
	"VIRTUALSERVER_CRYPT_ENC_PASS": "pass",
	"VIRTUALSERVER_EMAIL": "email",
	"VIRTUALSERVER_HOME": "home",
	"VIRTUALSERVER_FIELD_DEPLOYTYPE": "deploy_type",
	"VIRTUALSERVER_OWNER": "owner",
	"VIRTUALSERVER_DOM": "domain",
	#Database Variables
	"VIRTUALSERVER_DB": "dbname",
	# "VIRTUALSERVER_MYSQL_USER": "mysql_dbuser",
	"VIRTUALSERVER_POSTGRES_USER": "dbuser",
	"VIRTUALSERVER_POSTGRES_PASS": "dbpass", 
	"VIRTUALSERVER_FIELD_DBTYPE": "dbtype",
	#Authority Variables
	"VIRTUALSERVER_CREATOR": "creator_user",
}