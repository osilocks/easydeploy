#!/usr/bin/python
import os, sys

if __name__ == "__main__":
	"""
	Determine if we are in a virtualmin or cpanel environment
	"""
	from utils import _sysargs 

	args, kwargs = _sysargs() 
	servervars = __import__("servervars")

	if "action" in kwargs:
		"This should be the entry type"
		keys = servervars.entry_points["virtualmin"]  
		user_vars = dict((keys[key], value) for (key, value) in os.environ.items() if key in keys)
		if "django" in user_vars:
			#We want to deploy a django based website
			from author import UserDetails
			from automator import Automator
			a = Automator(kwargs["action"], user=UserDetails(**user_vars))